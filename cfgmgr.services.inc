<?php

/**
 * Enable a nginx vhost.
 */
function cfgmgr_nginx_enable() {
  $site_available = drush_get_option('confdir', '/') . 'etc/nginx/sites-available/' . drush_get_option('ns') . '.conf';
  $site_enabled   = str_replace('sites-available', 'sites-enabled', $site_available);

  if (realpath($site_enabled)) {
    return TRUE;
  }
  if (!symlink($site_available, $site_enabled)) {
    return drush_set_error("Site could not be enabled.");
  }
  return TRUE;
}

function cfgmgr_apache_enable() {
  $site_available = drush_get_option('confdir', '/') . 'etc/apache2/sites-available/' . drush_get_option('ns') . '.conf';
  $site_enabled   = str_replace('sites-available', 'sites-enabled', $site_available);

  if (realpath($site_enabled)) {
    return TRUE;
  }
  if (!symlink($site_available, $site_enabled)) {
    return drush_set_error("Site could not be enabled.");
  }
  return TRUE;
}

/**
 * Callback to create a PHP FPM user
 */
function cfgmgr_php5fpm_create_user() {
  if (!$user = drush_get_option('php-fpm-user', FALSE)) {
    return drush_set_error('php-fpm-user value no set. Please provide value as a drush option. e.g. --php-fpm-user=www-data');
  }
  if (!$group = drush_get_option('php-fpm-group', FALSE)) {
    return drush_set_error('php-fpm-group value no set. Please provide value as a drush option. e.g. --php-fpm-group=www-data');
  }

  // uids and gids can come from 3 different locations -
  // php5fpm.info, /etc/passwd and /etc/group
  $info = $passwd = $etcgroup = array();

  // Look for a shared file that details the users and group
  // uid and gid. Lack of this will incline to indicate that 
  // the user or group doesn't exist.
  $filename = DRUPAL_ROOT . '/sites/default/files/php5fpm.info';
  if (file_exists($filename)) {
    list($info['uid'], $info['gid']) = explode(':', trim(file_get_contents($filename)));
  }

  // Retrieve user information.
  drush_shell_exec('cat /etc/passwd | grep %s:x', $user); 
  
  foreach (drush_shell_exec_output() as $output_line) {
    if (strpos($output_line, $user) === 0) {
      list($user, $x, $passwd['uid'], $passwd['gid']) = explode(':', $output_line);
      break;
    }
  }

  // Retrieve group information.
  drush_shell_exec('cat /etc/group | grep %s:x:', $group);

  foreach (drush_shell_exec_output() as $output_line) {
    if (strpos($output_line, $group) === 0) {
      list($group, $x, $etcgroup['gid']) = explode(':', $output_line);
    }
  }

  // Test for the perfect condition where the user and group
  // are created and set in the .info file.
  if (isset($passwd['uid']) && isset($etcgroup['gid'])) {
    // If the info file doesn't exist, place them in there.
    if (empty($info)) {
      $info['uid'] = $passwd['uid'];
      $info['gid'] = $etcgroup['gid'];
    }
    // If $info isn't empty, then test that the uid and gids
    // are the same as those found.
    elseif ($passwd['uid'] != $info['uid'] || $etcgroup['gid'] != $info['gid']) {
      if ($passwd['uid'] != $info['uid']) {
        drush_set_error(dt("User '!user' (!uid) exists on the system already but has the wrong UID: !wrong_uid", array(
          '!user' => $user,
          '!uid'  => $info['uid'],
          '!wrong_uid' => $passwd['uid'],
        )));
      }
      if ($etcgroup['gid'] != $info['gid']) {
        drush_set_error(dt("Group '!group' (!gid) exists on the system already but has the wrong GID: !wrong_gid", array(
          '!group' => $group,
          '!gid'  => $info['gid'],
          '!wrong_gid' => $etcgroup['gid'],
        )));
      }
      // We've got issues, that we can't fix here. All we can do is exit.
      return FALSE;
    }
    // Otherwise, all is well. Return.
    else {
      return TRUE;
    }
  }
  else {
    // If the user doesn't exist, create it.
    if (!isset($passwd['uid'])) {
      // Figure out the user's uid.
      if (isset($info['uid'])) {
        $uid = $info['uid'];
      }
      // We need to create a new user uid. We going to put
      // all cfgmgr managed drupal php fpm users in the 600
      // range so start the uid at 600 and increment till we 
      // have an available uid to use.
      else {
        $uid = 600;
        while (drush_shell_exec('cat /etc/passwd | grep x:' .  $uid . ':')) {
          $uid++;
        }
      }
      $cmd = 'adduser --system --shell /bin/bash ';
      if (isset($info['uid']) && $info['uid'] == $info['gid']) {
        $cmd .= ' --group'; 
      }
      elseif (!isset($info['uid']) && $user == $group) {
        $cmd .= ' --group';
      }
      $cmd .= ' --uid %d %s';
      $cmd = sprintf($cmd, $uid, $user);
      if (!drush_shell_exec($cmd)) {
        drush_print(implode(PHP_EOL, drush_shell_exec_output()));
        drush_set_error("Failed to create new user '$user'");
        return FALSE;
      }
      $info['uid'] = $uid;
      // gid will be the same as uid if --group was added to the command.
      if (strpos($cmd, '--group')) {
        $info['gid'] = $uid;
        $group_created = TRUE;
      }
    }
    if (!isset($etcgroup['gid']) && !isset($group_created)) {
      // Like uids, gids will start at 600. We need to find a gid
      // that doesn't already exist.
      if (isset($info['gid'])) {
        $gid = $infp['gid'];
      }
      else {
        $gid = 600;
        while (drush_shell_exec('cat /etc/group | grep x:' . $gid . ':')) {
          $gid++;
        }
      }
      if (!drush_shell_exec(sprintf('addgroup --gid %d %s', $gid, $group))) {
        drush_print(implode(PHP_EOL, drush_shell_exec_output()));
        drush_set_error("Failed to create new group '$group'");
        return FALSE;
      }
      // Also need to associate the user with the group.
      if (!drush_shell_exec(sprintf('adduser %s %s', $user, $group))) {
        drush_print(implode(PHP_EOL, drush_shell_exec_output()));
        drush_set_error("Failed to add user '$user' to group '$group'");
        return FALSE;
      }
      $info['gid'] = $gid;
    }
  }
  drush_log("User '$user' and group '$group' successfully created.", 'success');
  drush_print("Please ensure that the Drupal files directory is writeable by the PHP-FPM user:", 2);
  drush_print("chown $user:$group -R site/default/files", 4);

  // Now that the user and group exist, create the info file if it doesn't already exist.
  if (!file_exists($filename)) {
    return file_put_contents($filename, implode(':', $info));
  }
  return TRUE;
}

/**
 * Setup the Drupal root.
 */
function cfgmgr_drupal_setup() {
  $failure = FALSE;
  if (!$sitedata = drush_get_option('drupal-sitedata', FALSE)) {
    drush_log("No sitedata configuration detailed. Omitting sitedata from Drupal setup.", 'warning');
  }
  elseif (!realpath($sitedata) && !mkdir($sitedata)) {
    drush_log("$sitedata is not a real directory and could not be created");
    $failure = TRUE;
  }
  else {
    // Symlink up files directory.
    if (!is_dir($sitedata . '/files') && !mkdir($sitedata . '/files')) {
      drush_log("Unable to create files directory under $sitedata", 'error');
    }
    $files_dir = realpath($sitedata . '/files');
    if (!realpath(DRUPAL_ROOT . '/sites/default/files') && !symlink($files_dir, DRUPAL_ROOT . '/sites/default/files')) {
      drush_log("Cannot symlink $files_dir to " . DRUPAL_ROOT . "/sites/default/files", 'error'); 
      $failure = TRUE;
    }
    else {
      drush_log(DRUPAL_ROOT . '/sites/default/files -> ' . realpath(DRUPAL_ROOT . '/sites/default/files'), 'status');
    }

    // Symlink up cache directory.
    if (!is_dir($sitedata . '/cache') && !mkdir($sitedata . '/cache')) {
      drush_log("Unable to create cache directory under $sitedata", 'error');
    }
    $cache_dir = realpath($sitedata . '/cache');
    if (!realpath(DRUPAL_ROOT . '/sites/default/cache') && !symlink($cache_dir, DRUPAL_ROOT . '/sites/default/cache')) {
      drush_log("Cannot symlink $cache_dir to " . DRUPAL_ROOT . "/sites/default/cache", 'error'); 
      $failure = TRUE;
    }
    else {
      drush_log(DRUPAL_ROOT . '/sites/default/cache -> ' . realpath(DRUPAL_ROOT . '/sites/default/cache'), 'status');
    }
  }
  if ($failure) {
    return drush_set_error("Drupal setup could not complete properly.");
  }
  return TRUE;
}
