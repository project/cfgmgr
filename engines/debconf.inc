<?php

/**
 * Debconf hander to deal with configuration of services.
 */
class cfg_handler_debconf extends cfg_handler_drushInteractive {

  protected $debconf;

  public function __construct($ns) {
    parent::__construct($ns);
    $this->debconf = new DebConf($ns); 
    $this->debconf_enabled = TRUE;
  }

  protected function debconf_get($key, $variable) {
    if (!$this->debconf_enabled) {
      return $this->whiptail($dc_key, $variable);
    }
    try {
      // Debconf variables use underscores rather than hyphens as a space seperator.
      $dc_key = $this->key_suffix . $key;
      if ($value = trim($this->debconf->get($dc_key))) {
        $variable['#default'] = $value;
      }

      // If the key hasn't been prompted infront of the user yet, use whiptail
      // to do so.
      if (!$this->debconf->fget($dc_key, 'seen')) {
        $value = $this->whiptail($dc_key, $variable);
        $this->debconf->set($dc_key, $value);

        // Now that the user has been prompted, set the seen flag to true.
        $this->debconf->fset($dc_key, 'seen', TRUE);
      }
    }
    catch (Exception $e) {
      drush_log($e->getMessage());
      drush_log('So such package available to debconf: ' . $this->ns . '. Reverting to whiptail only,', 'warning');
      $this->debconf_enabled = FALSE;
      $value = $this->whiptail($dc_key, $variable);
    }
    return $value;
  }

  public function ask($key, $variable) {
    return $this->translate($this->debconf_get($key, $variable));
  }

  public function confirm($key, $variable) {
    return $this->debconf_get($key, $variable);
  }

  public function select($key, $variable) {
    return $this->translate($this->debconf_get($key, $variable));
  }

  public function notify($key, $variable) {
    $message = $this->translate($this->debconf_get($key, $variable));
    drush_print($message);
    sleep(2);
    return NULL;
  }

  protected function whiptail($key, $variable) {
    if (!posix_isatty(STDIN)) {
      return drush_set_error("Standard in is not set to a tty.");
    }
    // Open a file to output result to.
    $filename = $tmpfname = tempnam("/tmp", $this->ns);
    $handle = fopen($filename, 'w+');

    // Find the file descriptor for the file.
    $fd = $this->fd($filename);

    // Set arguments for whiptail.
    $args = array(
      '--backtitle' => array('Drush Package configuration'),
      '--title' => array('Configuring ' . $this->ns . ' ' . $this->service),
      '--output-fd' => array($fd), 
      '--nocancel' => array(),
      '--defaultno' => array(),
    );
    $padding = 6;
    switch ($variable['#type']) {
      case 'select':
        $height = count($variable['#options']) + $padding + 4;
        $width = strlen($variable['#question']) + $padding;
        if (isset($variable['#default'])) {
          $args['--default-item'] = array($variable['#default']);
          unset($args['--defaultno']);
          $width = max(strlen($variable['#question']), strlen($variable['#default'])) + $padding;
        }
        $args['--noitem'] = array();
        $args['--menu'] = array($variable['#question'], $height, $width, count($variable['#options']));
        foreach ($variable['#options'] as $option) {
          $args['--menu'][] = $option['!key'];
          $args['--menu'][] = $option['!value'];
        }
        break;
      case 'ask':
        $args['--inputbox'] = array($variable['#question'], $padding + 4, strlen($variable['#question']) + $padding);
        if (isset($variable['#default'])) {
          $args['--inputbox'][] = $variable['#default'];
        }
        break;
      case 'confirm':
        $args['--yesno'] = array($variable['#question'], $padding + 4, strlen($variable['#question']) + $padding);
        if (isset($args['--default-item'])) {
          unset($args['--default-item']);
        }
        break;
      case 'notify':
        $args['--textbox'] = array($variable['#question'], $padding + 4, strlen($variable['#question']) + $padding);
        break;
      default:
        return isset($variable['#default']) ? $variable['#default'] : FALSE;
    }
    $cmd = 'whiptail';
    foreach ($args as $k => $values) {
      $cmd .= " $k";
      foreach ($values as $value) {
        $cmd .= ' ' . escapeshellarg($value);
      }
    }

    // Execute the whiptail command outputting the contents to the filehandler.
    passthru($cmd, $fail);

    // Close the file handler and read the contents.
    fclose($handle);
    $value = file_get_contents($filename);

    // Remove the file so old items don't linger.
    unlink($filename);

    if (!isset($args['--yesno']) && $fail) {
      drush_set_error($cmd);
      throw new Exception("Failed to invoke whiptail for $key");
    }
    elseif (isset($args['--yesno'])) {
      // Selecting 'no' with --yesno returns an exit status of 1. yes returns 0.
      // So the value must be reversed to get the real value.
      $value = !$fail;
    }

    return $value;
  }

  protected function fd($realpath) {
    $dir = '/proc/self/fd/';
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            $filename = $dir . $file;
            if (filetype($filename) == 'link' && realpath($filename) == $realpath) {
              closedir($dh);
              return $file;
            }
        }
        closedir($dh);
    }
    return FALSE;
  }

  /**
   * Reset stored variables from a namespace.
   */
  public function reset_variables($ns) {
    drush_shell_exec('debconf-show %s 2>/dev/null | grep ^\*', $ns);
    $ns = preg_quote($ns);
    foreach (drush_shell_exec_output() as $line) {
      preg_match('/^\* ' . $ns . '\/([^:]+):.*/', $line, $matches);
      $this->debconf->fset($matches[1], 'seen', FALSE);
    }
    return TRUE;
  }

}


/**
 * @file Library to talk to debconf.
 *
 * @author Josh Waihi <josh@catalyst.net.nz>
 */
class DebConf {

  /**
   * The package the retrieved values belong too.
   */
  protected $package;

  public function __construct($package) {
    $this->package = $package;
  }

  /**
   * Get a value from debconf. Alias to db_get.
   */
  public function get($name) {
    return $this->communicate("GET " . $this->package . '/' . $name);
  }

  /**
   * Set a value to debconf-communicate.
   */
  public function set($name, $value) {
    return $this->communicate("SET " . $this->package . "/$name $value");
  }

  public function fget($name, $flag) {
    $value = $this->communicate("FGET " . $this->package . "/$name $flag");
    return $value == 'true';
  }

  public function fset($name, $flag, $value = TRUE) {
    $value = $value == TRUE ? 'true' : 'false';
    return $this->communicate("FSET " . $this->package . "/$name $flag $value");
  }


  protected function communicate($cmd) {
    $arg = escapeshellarg($cmd);
    $value = exec('echo ' . $arg . ' | debconf-communicate');
    if (!preg_match('/^\d+.*$/i', $value)) {
      throw new Exception($value);
    }
    list($status, $return_value) = explode(' ', $value, 2);
    // Non zero status means an unsuccessful command.
    if ("$status" !== "0") {
      throw new Exception($value);
    }
    return $return_value;
  }
}

