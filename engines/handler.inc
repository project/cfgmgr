<?php

/**
 * Base handler to deal with configuration of services.
 */
abstract class cfg_handler {

  /**
   * Namespace like drupal-site-[project].
   */
  protected $ns = '';

  /**
   * Store values already retrieved so that they are not retrieved again.
   */
  protected $cache = array();

  /**
   * Service name.
   */
  protected $service = '';

  /**
   * Key suffix.
   */
  protected $key_suffix = '';

  /**
   * A service's variable registry.
   */
  protected $registry = array();

  public function __construct($ns = '') {
      $this->ns = $ns;
  }

  /**
   * Magic method: Retrieve a services variable value.
   */
  public function __get($key) {
    // Make ns publically readable.
    if ($key == 'ns') { return $this->ns; }

    return $this->get_value($key, $this->registry);
  }

  /**
   * Magic method helper function for get a value.
   */
  protected function get_value($key, $registry) {
    // If the key doesn't exist, throw an error.
    if (!isset($registry[$key])) { drush_print_r($registry); return $this->key_error("[$this->service]: No such key: $key"); }

    $cache_key = $this->key_suffix . $key;

    if (($cache = $this->cache_get($cache_key)) !== NULL) { return $cache; }

    if (!method_exists($this, $registry[$key]['#type'])) {
      return $this->key_error("[$cache_key]: No such variable type for $key: " . $registry[$key]['#type']);
    }

    drush_log("getting value for $key of type " . $registry[$key]['#type']);

    if ($registry[$key]['#type'] == 'group') {
      $old_key_suffix = $this->key_suffix;
      $this->key_suffix .= $key . '_';
    }

    $value = call_user_func(array($this, $registry[$key]['#type']), $key, $registry[$key]);

    if ($registry[$key]['#type'] == 'group') {
      $this->key_suffix = $old_key_suffix;
    }

    $this->cache_set($cache_key, $value);

    return $value;
  }

  protected function cache_get($key) {
    if (isset($this->cache[$key])) {
      return $this->cache[$key];
    }
  }

  protected function cache_set($key, $value) {
    return $this->cache[$key] = $value;
  }

  /**
   * @ingroup {
   * Abstract callback functions.
   */

  /**
   * @return array of values.
   */
  public function group($key, $variable) {
    $value = array();
    foreach ($this->element_children($variable) as $child) {
      $value[$child] = $this->get_value($child, $variable);
    }

    // Indicator of multiple indexes of each value. As DrushInteractive
    // doesn't have predefined values it can only get values for the first
    // key in the array.
    if ($variable['#group_type'] == 'multiple') {
      $value = array($value);
    }
    return $value;
  }

  /**
   * @return string value.
   */
  public abstract function ask($key, $variable); 

  /**
   * @return boolean.
   */
  public abstract function confirm($key, $variable); 

  /**
   * @return NULL.
   */
  public abstract function notify($key, $variable); 

  /**
   * @return string value.
   */
  public abstract function select($key, $variable); 
  /**
   * } @ingroup
   */

  /**
   * Issue an error.
   */
  public abstract function key_error($message); 

  /**
   * Register a services variables.
   */
  public function registry_variables($variables = FALSE) {
    if ($variables) {
      foreach ($variables as $key => &$value) {
        $this->process_registry_variable($key, $value);
      }
      $this->registry = $variables; 
    }
    return $this->registry;
  }

  protected function process_registry_variable($variable_name, &$options) {
    if ($children = $this->element_children($options)) {
      $options['#type'] = 'group';
    }
    foreach ($children as $child_key) {
      $this->process_registry_variable($child_key, $options[$child_key]);
    }
    if (isset($options['#default'])) {
      $options['#default'] = str_replace('!ns', $this->ns, $options['#default']);
    }

  }

  public function setService($service) {
    $this->service = $service;
    $this->key_suffix = $service . '_';
  }

  /**
   * Reset stored variables from a namespace.
   *
   * As Drush is stateless, just return true.
   */
  public function reset_variables($ns, $service) {
    return TRUE;
  }

  /**
   * Copied from Drupal core. Get children of parent registry keys.
   */
  protected function element_children(&$elements, $sort = FALSE) {
    // Do not attempt to sort elements which have already been sorted.
    $sort = isset($elements['#sorted']) ? !$elements['#sorted'] : $sort;

    // Filter out properties from the element, leaving only children.
    $children = array();
    $sortable = FALSE;
    foreach ($elements as $key => $value) {
      if ($key === '' || $key[0] !== '#') {
        $children[$key] = $value;
        if (is_array($value) && isset($value['#weight'])) {
          $sortable = TRUE;
        }
      }
    }
    // Sort the children if necessary.
    if ($sort && $sortable) {
      uasort($children, 'element_sort');
      // Put the sorted children back into $elements in the correct order, to
      // preserve sorting if the same element is passed through
      // element_children() twice.
      foreach ($children as $key => $child) {
        unset($elements[$key]);
        $elements[$key] = $child;
      }
      $elements['#sorted'] = TRUE;
    }
    return array_keys($children);
  }

  /**
   * Translate a string variable.
   */
  public function translate($value) {
    return strtr($value, array(
      '!ns' => $this->ns,
    ));
  }
}
