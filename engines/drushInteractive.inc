<?php

/**
 * Base handler to deal with configuration of services.
 */
class cfg_handler_drushInteractive extends cfg_handler {

  public function ask($key, $variable) {
    $message = "[{$this->key_suffix}{$key}] " . $variable['#question'];
    if (!isset($variable['#default']) || $variable['#default'] == '') {
      $variable['#default'] = 'none';
      if (($value = drush_prompt($message, $variable['#default'])) == 'none') {
        return '';
      }
    }
    else {
      $value = drush_prompt($message, $variable['#default']);
    }
    return $this->translate($value);
  }

  public function confirm($key, $variable) {
    $message = "[{$this->key_suffix}{$key}] " . $variable['#question'];
    return drush_confirm($message);
  }

  public function select($key, $variable) {
    $message = "[{$this->key_suffix}{$key}] " . $variable['#question'];
    $key = drush_choice($variable['#options'], $message, $variable['#default']);
    return $this->translate($variable['#options'][$key]['!key']);
  }

  public function notify($key, $variable) {
    $message = "[{$this->key_suffix}{$key}] " . $variable['#question'];
    drush_print($message);
    sleep(2);
    return NULL;
  }

  protected function cache_get($key) {
    $drush_option_key = str_replace('_', '-', $key);
    if (($value = drush_get_option($drush_option_key, NULL)) !== NULL) {
      return $value;
    }
    return parent::cache_get($key);
  }

  public function key_error($message) {
    drush_set_error($message);
  }
}
