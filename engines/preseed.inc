<?php

/**
 * Preseed hander to deal with configuration of services.
 */
class cfg_handler_preseed extends cfg_handler_drushInteractive {

  protected $values = array();

  public function __construct($ns) {
    parent::__construct($ns);

    $preseed_file = drush_get_option(array('file', 'preseed-file'), drush_get_option('confdir', '/') . 'etc/' . $ns . '/preseed.ini');

    if (!file_exists($preseed_file)) {
      return drush_set_error("No preseed file exists at $preseed_file.");
    }

    foreach (file($preseed_file) as $line) {
      $line = trim($line);
      if (empty($line)) {
        continue;
      }
      if (strpos($line, ';') !== FALSE) {
        continue;
      }
      if (preg_match_all('/^\[([\w\d_\:-]+)\]$/', $line, $matches)) {
        $key = $matches[1][0];
        $this->values[$key] = array();
        continue;
      }
      if (preg_match_all('/^(.*)\s*=(.*)$/U', $line, $matches)) {
        if (!isset($key)) {
          return drush_set_error("INI parse error. variable not under a section");
        }
        $value = preg_replace('/^"|\'/', '', trim($matches[2][0]));
        $value = preg_replace('/"|\'$/', '', $value);
        $this->parse_ini_value($this->values[$key], $matches[1][0], $value);
      }
    }
  }

  protected function parse_ini_value(&$values, $key, $value) {
    $key = str_replace(']', '', $key);
    if (strpos($key, '[') === FALSE) {
      $values[$key] = $value;
      return;
    }
    $main_key = substr($key, 0, strpos($key, '['));
    $sub_keys = substr($key, strpos($key, '[') + 1);

    if (!isset($values[$main_key])) {
      $values[$main_key] = array();
    }

    $new_value = &$values[$main_key];
    $this->parse_ini_value($new_value, $sub_keys, $value);
  }

  public function ask($key, $variable) {
    return $this->translate($this->values[$this->service][$key]);
  }

  public function confirm($key, $variable) {
    return (bool) $this->values[$this->service][$key];
  }

  public function select($key, $variable) {
    return $this->translate($this->values[$this->service][$key]);
  }

  public function notify($key, $variable) {
    drush_print($this->values[$this->service][$key]);
    sleep(2);
  }

  public function group($key, $variable) {
    return $this->values[$this->service][$key];
  }

}

