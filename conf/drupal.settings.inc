<?php

class cfgmgr_DrupalSettingsConf extends cfgmgr_Conf {

  public $service = 'drupal';

  protected $cfg_registry = array(
    'db' => array(
      '#type' => 'group',
      '#group_type' => 'multiple',
      'host' => array(
        '#type' => 'ask',
        '#question' => 'What is the name of the database server?',
        '#default' => 'localhost',
      ),
      'driver' => array(
        '#type' => 'select',
        '#question' => 'What type of databse does Drupal use?',
        '#options' => array(
          array('!key' => 'pgsql', '!value' => 'PostgreSQL'),
          array('!key' => 'mysql', '!value' => 'MySQL'),
        ),
      ),
      'databasename' => array(
        '#type' => 'ask',
        '#question' => 'What is the name of the Database',
        '#default' => '!ns',
      ),
      'username' => array(
        '#type' => 'ask',
        '#question' => 'What is the username for the database user',
        '#default' => '!ns',
      ),
      'password' => array(
        '#type' => 'ask',
        '#question' => 'What is the password for the database user? Leave blank for none.',
      ),
      'port' => array(
        '#type' => 'ask',
        '#question' => 'What is the database port? leave empty to assume default',
      ),
      'prefix' => array(
        '#type' => 'ask',
        '#question' => 'What is the prefix for the database? Leave empty for none.',
        '#default' => '',
      ),
    ),
    'root' => array(
      '#type' => 'ask',
      '#question' => 'Where is the Drupal root?',
    ),
    'version' => array(
      '#type' => 'select',
      '#question' => 'Which version of Drupal is this project',
      '#options' => array(
        array('!key' => '6', '!value' => 'Drupal 6'),
        array('!key' => 7, '!value' => 'Drupal 7'),
      ),
    ),
    'sitedata' => array(
      '#type' => 'ask',
      '#question' => 'This configuration tool supports sitedata partions to symlink to sites/default/files and sites/default/cache. List the directory below are leave empty to disable',
      '#default' => '/var/lib/sitedata/!ns',
    ),
  );

  public function __construct($cfg_handler) {
    if (defined('DRUPAL_ROOT')) {
      $this->cfg_registry['root']['#default'] = DRUPAL_ROOT;
    }
    parent::__construct($cfg_handler);
  }

  public function conf() {
    // Set the Drupal root.
    if (!$root = drush_get_option(array('r', 'root'))) {
      $root = $this->root;
    }

    // Load the contents of the file.
    if (!$settings = file_get_contents($root . '/sites/default/default.settings.php')) {
      return drush_set_error("Could not find default.settings.php in $root/sites/default file.");
    }

    $database = array();
    // Generate the database connection credentials.
    foreach ($this->db as $key => $connection_info) {
      if ($key == 'default' || (is_numeric($key) && $key == 0)) {
        $databases['default'] = $connection_info;
        continue;
      }
      // As the default connection will be the first key in the
      // array, to keep the slave array clean, start its count from 0.
      if (is_numeric($key)) {
        $key--;
      }
      $databases['slave'][$key] = $connection_info;
    }

    // Drupal 7 and greater user database arrays and are formatted
    // different to eariler versions.
    if ($this->version >= 7) {
      // Slaves cannot have keys, they must be numerically
      // indexed and sequenced.
      if (isset($databases['slave'])) {
        $databases['slave'] = array_values($databases['slave']);
      }
      $db_settings = '$databases = ' . var_export(array('default' => $databases), TRUE) . ';' . PHP_EOL;

      // We replace the default empty databases array. If its not there then
      // we can't add the settings.
      if (strpos($settings, "\n\$databases = array();") === FALSE) {
        return drush_set_error("Could not place database settings. Template default.settings.php is not standard.");
      }

      // Find and replace the database connection details..
      $settings = str_replace("\n\$databases = array();", "\n$db_settings", $settings);
    }
    else {
      // Set the first database connection as the default connection.
      $db_url['default'] = strtr('driver://username:password@host:port/databasename', $databases['default']);

      // Add the slave databases (if any).
      if (isset($databases['slave'])) {
        foreach ($databases['slave'] as $idx => $connection_info) {
          $key = is_numeric($idx) ? 'slave' . $idx : $idx;
          $db_url[$key] = strtr('driver://username:password@host:port/databasename', $connection_info);
        }
      }

      // Format the database url variable.
      $db_url = '$db_url = ' . var_export($db_url, TRUE) . ';' . PHP_EOL;

      // Place the exported variable into settings.php.
      if (strpos($settings, "\n\$db_url = 'mysql://username:password@localhost/databasename';") === FALSE) {
        return drush_set_error("Could not place database settings. Template default.settings.php is not standard."); 
      }
      $settings = str_replace("\n\$db_url = 'mysql://username:password@localhost/databasename';", "\n$db_url", $settings);

      // The database prefix in Drupal 6 and lower is global and needs to be 
      // done seperately.
      if ($databases['default']['prefix']) {
        $prefix = '$db_prefix = "' . $databases['default']['prefix'] . '";';
        if (strpos($settings, "\n\$db_prefix = '';") === FALSE) {
          return drush_set_error("Could not place database prefix. Template default.settings.php is not standard.");   
        }
        $settings = str_replace("\n\$db_prefix = '';", "\n$prefix", $settings);
      }
    }

    // Retrieve the sitedata value for service.setup.drupal.
    $this->sitedata;
    return $settings;
  }
}
