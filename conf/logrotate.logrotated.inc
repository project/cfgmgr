<?php

class cfgmgr_LogrotateLogrotatedConf extends cfgmgr_Conf {

  public $service = 'logrotate';

  public function conf($contents) {
    $registry = drush_command_invoke_all('cfgmgr_registry');

    // Use template if file doesn't yet exist.
    if (!$contents) {
      $contents = file_get_contents(dirname(__FILE__) . '/logrotate.logrotated.conf');
    }

    $subs = $reload = $weblogs = $cronlogs = array();

    // Check for Nginx and Apache log file settings being set by Drush.
    // Nginx and apache use log rotate as an 'on install' event to call
    // to setup the log files to rotate.
    foreach (array('nginx', 'apache') as $service) {
      if ($log = drush_get_option($service . '-error_log', FALSE)) {

        // Ensure the service is reloaded after a log is rotated.
        $reload[$service] = $registry[$service]['actions']['reload']['command'];

        // Only add logs to rotate if they haven't already been setup to rotate.
        if (strpos($contents, $log) === FALSE) {
          $weblogs[] = $log;
        }
      }
      if ($log = drush_get_option($service . '-access_log', FALSE)) {

        // Ensure the service is reloaded after a log is rotated.
        $reload[$service] = $registry[$service]['actions']['reload']['command'];

        // Only add logs to rotate if they haven't already been setup to rotate.
        if (strpos($contents, $log) === FALSE) {
          $weblogs[] = $log;
        }
      }
    }

    // Add any new weblogs to the file but remove if not present.
    // preserve the placeholder to be used again.
    if (count($weblogs)) {
      // Ensure the placholder is returned to the conf file
      // once we replace it so this script can be run on it again.
      $weblogs[] = '# __web_log_placeholder__#';
      $subs['# __web_log_placeholder__#'] = implode(PHP_EOL, $weblogs);
      $subs['##WEB##'] = '';
      $cmds = array();
      foreach ($reload as $service => $command) {
        if (strpos($contents, $command) === FALSE) {
          $cmds[] = "     $command > /dev/null";
        }
      }
      if (!empty($cmds)) {
        // Ensure the placholder is returned to the conf file
        // once we replace it so this script can be run on it again.
        $cmds[] = '#######      __service_reload_placeholder';
        $subs['#######      __service_reload_placeholder'] = implode(PHP_EOL, $cmds);
      }
    }
    
    // Support rotating cron logs.
    if ($log = drush_get_option('cron-log', FALSE)) {
      if (strpos($contents, $log) === FALSE) {
        $cronlogs[] = $log;
        // Ensure the placholder is returned to the conf file
        // once we replace it so this script can be run on it again.
        $cronlogs[] = '#__cron_log_placeholder__#';
        $subs['#__cron_log_placeholder__#'] = implode(PHP_EOL, $cronlogs);
        $subs['##CRON##'] = '';
      }
    }

    // Support PHP FPM log rotating.
    if ($log = drush_get_option('php-fpm-error_log', FALSE)) {
      if (strpos($contents, $log) === FALSE) {
        $phplogs[] = $log;
        // Ensure the placholder is returned to the conf file
        // once we replace it so this script can be run on it again.
        $phplogs[] = '#__php-fpm_log_placeholder__#';
        $subs['#__php-fpm_log_placeholder__#'] = implode(PHP_EOL, $phplogs);
        $subs['##PHP-FPM##'] = '';
      }
    }
    return strtr($contents, $subs);
    
  }


}
