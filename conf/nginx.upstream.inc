<?php

class cfgmgr_NginxUpstreamConf extends cfgmgr_Conf {

  public $service = 'nginx';

  protected $cfg_registry = array(
    'upstreams' => array(
      '#type' => 'ask',
      '#question' => 'Name a comma seperated list of upstreams as socket or host references. E.g web1.server.com:80 or unix:/var/run/php-fpm.socket.',
      '#default' => 'unix:/var/run/!ns.socket',
     ),
    'name' => array(
      '#type' => 'ask',
      '#question' => 'What is the name of this upstream',
      '#default' => '!ns',
    ),
  );

  public function conf() {
    $upstream = file_get_contents(dirname(__FILE__) . '/upstream_phpcgi.conf');
    $replacements['!upstream_name'] = $this->name;
    $upstreams = '';
    foreach (explode(',', $this->upstreams) as $upstream_server) {
      $upstream_server = trim($upstream_server);
      $upstreams .= 'server ' . $upstream_server . ';' . PHP_EOL;
    }
    $replacements['%upstreams%'] = $upstreams;
    return strtr($upstream, $replacements);
  }

}
