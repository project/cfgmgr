<?php

class cfgmgr_ApacheVhostConf extends cfgmgr_Conf {

  public $service = 'apache';

  protected $cfg_registry = array(
    // Port
    'port' => array(
      '#type' => 'ask',
      '#question' => "What is the port your vhost should listen on?",
      '#default' => 80,
    ),
    'ssl_port' => array( 
      '#type' => 'ask',
      '#question' => "What is the SSL port your vhost should listen on?",
      '#default' => 443,
    ), 
    // SSL settings
    'ssl_support' => array(
      '#type' => 'confirm',
      '#question' => "Do you want to support SSL",
      '#default' => FALSE,
    ),
    // IP
    'ssl_ip' => array(
      '#type' => 'ask',
      '#question' => "What is the SSL IP your vhost should listen on?",
    ),
    // Path to SSL cert
    'ssl_cert' => array(
      '#type' => 'ask',
      '#question' => "Where is the SSL cert located on the server",
      '#default' => '/etc/ssl/certs/!ns-cert.pem',
    ),
    // Path to SSL cert
    'ssl_cert_key' => array(
      '#type' => 'ask',
      '#question' => "Where is the SSL cert key located on the server",
      '#default' => '/etc/ssl/certs/!ns.key',
    ),
    // Server name details
    'servername' => array(
      '#type' => 'ask',
      '#question' => "What is the servername for this vhost",
    ),
    'serveraliases' => array(
      '#type' => 'ask',
      '#question' => "Do you want to support any server aliases? If so name each one seperated by a space",
    ),
    'serverredirects' => array(
      '#type' => 'ask',
      '#question' => "Name any domains that should redirect to the servername seperated by spaces",
    ),
    'webroot' => array(
      '#type' => 'ask',
      '#question' => "Where is the web root for this virtual host?",
      '#default' => '/var/www/drupal/!ns',
    ),
  );

  public function __construct($cfg_handler) {
    $this->cfg_registry['servername']['#default'] = exec('hostname -f');
    if (defined('DRUPAL_ROOT')) {
      $this->cfg_registry['webroot']['#default'] = DRUPAL_ROOT;
    }
    parent::__construct($cfg_handler);
  }

  /**
   * Generate a vhost configuration file.
   */
  public function conf() {
    $vhost = file_get_contents(dirname(__FILE__) . '/apache2.example.com.conf');

    if (strpos($this->servername, 'www') !== 0) {
      $this->cfg_registry['serverredirects']['#default'] = 'www.' . $this->servername;
      $this->cfg_handler->registry_variables($this->cfg_registry);
    }
    drush_set_option($this->service . '-error_log', '/var/log/sitelogs/!ns/error.log'); 
    drush_set_option($this->service . '-access_log', '/var/log/sitelogs/!ns/access.log'); 

    $replacements = array(
      'example.com' => $this->servername, 
      '!ns' => $this->cfg_handler->ns,
      'ServerName example.com' => 'ServerName ' . $this->servername,
      '!redirects' => $this->serverredirects,
      '*:80' => '*:' . $this->port, 
      'CustomLog /var/log/sitelogs/!ns/access.log' => 'CustomLog ' . drush_get_option($this->service . '-access_log'),
      'ErrorLog  /var/log/sitelogs/!ns/error.log' => 'ErrorLog ' . drush_get_option($this->service . '-error_log'),
      '/var/www/drupal/!ns' => $this->webroot,
      'ServerAlias www.example.com' => empty($this->serveraliases) ? '' : 'ServerAlias ' . $this->serveraliases,
    );

# TODO:
#    if ($this->ssl_support) {
#      $vhost .= file_get_contents(dirname(__FILE__) . '/ssl.example.com.conf');
#      $replacements['[::]:443 ssl'] = '[::]:' . $this->ssl_port . ' ssl';
#      $replacements['!ssl_ip'] = $this->ssl_ip;
#      $replacements['/etc/ssl/certs/example-cert.pem'] = $this->ssl_cert;
#      $replacements['/etc/ssl/private/example.key'] = $this->ssl_cert_key;
#    }

    return strtr($vhost, $replacements);
  }
}
