<?php

abstract class cfgmgr_Conf {

  protected $cfg_handler = NULL;

  protected $cfg_registry = array();

  public $service = '';

  public function __construct($cfg_handler) {
    $this->cfg_handler = $cfg_handler;
    drush_command_invoke_all_ref('cfgmgr_registry_alter', $this->cfg_registry, $this->service);
    $this->cfg_handler->registry_variables($this->cfg_registry);
  }

  /**
   * Magic Method: shortcut to lazy load variables needed.
   */
  public function __get($key) {
    return $this->cfg_handler->$key;
  }

  /**
   * Read only access to $cfg_handler
   */
  public function cfg() {
    return $this->cfg_handler;
  }

  public function pre_render() {
    drush_command_invoke_all('cfgmgr_pre_render', $this);
  }

  public function post_render(&$contents) {
    drush_command_invoke_all_ref('cfgmgr_post_render', $contents, $this);
  }
}
