<?php

class cfgmgr_PhpfpmPoolConf extends cfgmgr_Conf {

  public $service = 'php-fpm';

  protected $cfg_registry = array(
    'pool' => array(
      '#type' => 'ask',
      '#question' => 'What is the name of the PHP FPM pool?',
      '#default' => '!ns',
    ),
    'listen' => array(
      '#type' => 'ask',
      '#question' => 'Name the socket or IP and port that this pool will listen on',
      '#default' => '/var/run/!ns.socket',
    ),
    'user' => array(
      '#type' => 'ask',
      '#question' => 'What is the Unix username the pool will run as',
      '#default' => '!ns',
    ),
    'group' => array(
      '#type' => 'ask',
      '#question' => 'What is the Unix group the pool will run under. This should be a common group shared with the webserver (often called www-data)',
      '#default' => 'www-data',
    ),
    // php admin values
    'display_errors' => array(
      '#type' => 'confirm',
      '#question' => 'Display PHP errors?',
      '#default' => FALSE,
    ),
    'error_log' => array(
      '#type' => 'ask',
      '#question' => 'Error Log for PHP FPM',
      '#default' => '/var/log/sitelogs/!ns/php_error.log',
    ),
    // pm values
    'pm' => array(
      '#type' => 'select',
      '#question' => 'Should FPM run in static or dynamic mode?',
      '#options' => array(
        array('!key' => 'dynamic', '!value' => 'Dynamic'),
        array('!key' => 'static', '!value' => 'Static'),
      ),
      '#default' => 'dynamic',
    ),
    'max_children' => array(
      '#type' => 'ask',
      '#question' => 'The maximum number of child processes to be created',
      '#default' => 6,
    ),
    'start_servers' => array(
      '#type' => 'ask',
      '#question' => 'The number of child processes created on startup',
      '#default' => 3,
    ),
    'min_spare_servers' => array(
      '#type' => 'ask',
      '#question' => 'The desired minimum number of idle server processes',
      '#default' => 3,
    ),
    'max_spare_servers' => array(
      '#type' => 'ask',
      '#question' => 'The desired maximum number of idle server processes',
      '#default' => 5,
    ),
    'max_requests' => array(
      '#type' => 'ask',
      '#question' => 'The number of requests each child process should execute before respawning',
      '#default' => 500,
    ),
  );

  public function conf() {
    $poolconf = file_get_contents(dirname(__FILE__) . '/php-fpm.pool.conf');

    return strtr($poolconf, array(
      'www' => $this->pool,
      'www-data' => $this->user,
      'group = www-data' => 'group = ' . $this->group,
      'listen.group = www-data' => 'listen.group = ' . $this->group,
      'listen.mode = 0600' => 'listen.mode = 0660',
      'listen = /var/run/php-fpm.sock' => 'listen = ' . $this->listen,
      ';php_admin_value[error_log] = /var/log/fpm-php.www.log' => 'php_admin_value[error_log] = ' . $this->error_log,
      ';php_flag[display_errors] = off' => 'php_flag[display_errors] = ' . ($this->display_errors ? 'on' : 'off'),
      ';pm = dynamic' => 'pm = ' . $this->pm,
      ';pm.max_children = 6' => 'pm.max_children = ' . $this->max_children,
      ';pm.start_servers = 3' => 'pm.start_servers = ' . $this->start_servers,
      ';pm.min_spare_servers = 3' => 'pm.min_spare_servers = ' . $this->min_spare_servers,
      ';pm.max_spare_servers = 5' => 'pm.max_spare_servers = ' . $this->max_spare_servers,
      ';pm.max_requests = 500' => 'pm.max_requests = ' . $this->max_requests,
    ));
  }

}
