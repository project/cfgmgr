<?php

class cfgmgr_CronCrondConf extends cfgmgr_Conf {

  public $service = 'cron';

  protected $cfg_registry = array(
    'user' => array(
      '#type' => 'ask',
      '#question' => 'Which user should run Drupal\'s cron? This should be the user that serves Drupal requests to the webserver.',
      '#default' => '!ns',
    ),
    'servername' => array(
      '#type' => 'ask',
      '#question' => 'What is the server name this site should run cron under?',
    ),
    'log' => array(
      '#type' => 'ask',
      '#question' => 'Where should the cron logs be placed?',
      '#default' => '/var/log/sitelogs/!ns/cron.log',
    ),
    'frequency' => array(
      '#type' => 'select',
      '#question' => 'How frequent should cron run?',
      '#options' => array(
        array('!key' => 'disabled', '!value' => 'Disabled'),
        array('!key' => 2, '!value' => 'Every 2 Minutes'),
        array('!key' => 3, '!value' => 'Every 3 Minutes'),
        array('!key' => 5, '!value' => 'Every 5 Minutes'),
        array('!key' => 10, '!value' => 'Every 10 Minutes'),
        array('!key' => 12, '!value' => 'Every 12 Minutes'),
        array('!key' => 15, '!value' => 'Every 15 Minutes'),
        array('!key' => 20, '!value' => 'Every 20 Minutes'),
        array('!key' => 30, '!value' => 'Every 30 Minutes'),
        array('!key' => 45, '!value' => 'Every 45 Minutes'),
        array('!key' => 60, '!value' => 'Every Hour'),
        array('!key' => 1440, '!value' => 'Every Day'),
      ),
    ),
    'offset' => array(
      '#type' => 'ask',
      '#question' => 'State an offset from the hour from which cron will imply its frequency. This must be a value between 0 and 59', 
      '#default' => 0,
    ),
    'root' => array(
      '#type' => 'ask',
      '#question' => 'Where is the root of the Drupal installation',
    ),
  );

  public function __construct($cfg_handler) {
    $this->cfg_registry['root']['#default'] = drush_get_option(array('root', 'r'), '/var/www/drupal/!ns');
    parent::__construct($cfg_handler);
  }

  public function conf() {
    $cron_command = 'min hr day week month user cmd' . PHP_EOL;
    $info = array(
      'min' => '4-59/12',
      'hr' => '*',
      'day' => '*',
      'week' => '*',
      'month' => '*',
      'user' => $this->user,
      'cmd' => '(echo -n "[`date`] " && drush -r ' . $this->root . ' -u 1 -l ' . $this->servername . ' --nocolor cron) >> ' . $this->log . ' 2>&1',
    );
    if ($this->frequency == 'disabled') {
      return '# Cron for ' . $this->ns . ' is disabled. Run \'drush --ns=' . $this->ns . ' --confdir=' . drush_get_option('confdir', '/') . ' --cfg_handler=' . drush_get_option('cfg_handler', 'drushInteractive') . ' cfgmgr conf.crond.cron\' to enable.' . PHP_EOL;
    }

    if ($this->frequency > 59) {
      $info['min'] = $this->offset;
      if ($this->frequency == 1440) {
        // Make cron run in the morning as most sites will be quite then.
        $info['hr'] = 2;
      }
    }
    else {
      $info['min'] = '';
      if ($min = $this->offset % $this->frequency) {
        while ($min < $this->offset) {
          $info['min'] .= "$min,";
          $min += $this->frequency;
        }
      }
      $info['min'] .= $this->offset . '-59/' . $this->frequency;
    }

    return strtr($cron_command, $info);
  }

}
