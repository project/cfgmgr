<?php

class cfgmgr_NagiosNRPEConf extends cfgmgr_Conf {

  protected $cfg_registry = array(
    'drush_bin' => array(
      '#type' => 'ask',
      '#question' => 'Where is the Drush executable located, used for nagios checks?',
      '#default' => '/usr/bin/drush',
    ),
  );

  public function conf() {
    $commands = array_merge(module_invoke_all('nagios_checks'), drush_command_invoke_all('nagios_checks')); 
    $command_prefix = str_replace('-', '_', $this->ns);
    $output = '';
    foreach ($commands as $cmd => $desc) {
      $output .= '# ' . $desc . PHP_EOL;
      $output .= 'command[' . $command_prefix . '_' . $cmd . ']=' . $this->drush_bin . ' -r ' . DRUPAL_ROOT . ' nagios-check ' . $cmd . PHP_EOL . PHP_EOL;
    }

    return $output;
  }

}
