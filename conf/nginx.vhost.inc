<?php

class cfgmgr_NginxVhostConf extends cfgmgr_Conf {

  public $service = 'nginx';

  protected $cfg_registry = array(
    // Port
    'port' => array(
      '#type' => 'ask',
      '#question' => "What is the port your vhost should listen on?",
      '#default' => 80,
    ),
    // IPv6 settings
    'ipv6_support' => array(
      '#type' => 'confirm',
      '#question' => "Do you want to support IPv6?",
      '#default' => FALSE,
    ),
    // IP
    'ipv6_ip' => array(
      '#type' => 'ask',
      '#question' => "What is the IPv6 ip your vhost should listen on?",
      '#default' => '[::]',
    ),
    // SSL settings
    'ssl_support' => array(
      '#type' => 'confirm',
      '#question' => "Do you want to support SSL?",
      '#default' => FALSE,
    ),
    // Port
    'ssl_port' => array(
      '#type' => 'ask',
      '#question' => "What is the SSL port your vhost should listen on?",
      '#default' => 443,
    ),
    // IP
    'ssl_ip' => array(
      '#type' => 'ask',
      '#question' => "What is the SSL IP your vhost should listen on?",
    ),
    // Path to SSL cert
    'ssl_cert' => array(
      '#type' => 'ask',
      '#question' => "Where is the SSL cert located on the server",
      '#default' => '/etc/ssl/certs/!ns-cert.pem',
    ),
    // Path to SSL key
    'ssl_cert_key' => array(
      '#type' => 'ask',
      '#question' => "Where is the SSL cert key located on the server",
      '#default' => '/etc/ssl/certs/!ns.key',
    ),
    // Server name details
    'servername' => array(
      '#type' => 'ask',
      '#question' => "What is the servername for this vhost",
    ),
    'serveraliases' => array(
      '#type' => 'ask',
      '#question' => "Do you want to support any server aliases? If so name each one seperated by a space",
    ),
    'serverredirects' => array(
      '#type' => 'ask',
      '#question' => "Name any domains that should redirect to the servername seperated by spaces",
    ),
    'webroot' => array(
      '#type' => 'ask',
      '#question' => "Where is the web root for this virtual host?",
      '#default' => '/var/www/drupal/!ns',
    ),
    // Upstream Reference
    'upstream' => array(
      '#type' => 'ask',
      '#question' => "What will the Upstream name be that Nginx will forward Drupal requests onto?",
      '#default' => '!ns',
    ),
    'upstream_type' => array(
      '#type' => 'select',
      '#question' => 'Which upstream method is being used?',
      '#options' => array(
        array('!key' => 'fcgi', '!value' => 'PHP FastCGI (FPM)'),
        array('!key' => 'apache', '!value' => 'Proxy to Apache')
      ),
      '#default' => 'fcgi',
    ),
  );

  public function __construct($cfg_handler) {
    $this->cfg_registry['servername']['#default'] = exec('hostname -f');
    if (defined('DRUPAL_ROOT')) {
      $this->cfg_registry['webroot']['#default'] = DRUPAL_ROOT;
    }
    parent::__construct($cfg_handler);
  }

  /**
   * Generate a vhost configuration file.
   */
  public function conf() {
    $vhost = file_get_contents(dirname(__FILE__) . '/nginx.example.com.conf');

    /* Set a default redirect of www.<servername>
    if (strpos($this->servername, 'www') !== 0) {
      $this->cfg_registry['serverredirects']['#default'] = 'www.' . $this->servername;
      $this->cfg_handler->registry_variables($this->cfg_registry);
    }
    //*/
    drush_set_option($this->service . '-error_log', '/var/log/sitelogs/!ns/nginx_error.log'); 
    drush_set_option($this->service . '-access_log', '/var/log/sitelogs/!ns/nginx_access.log'); 

    $replacements = array(
      'example.com' => $this->servername, 
      '!ns' => $this->cfg_handler->ns,
      'server_name example.com' => 'server_name ' . $this->servername . ' ' . $this->serveraliases,
      '!redirects' => $this->serverredirects,
      'listen 80' => 'listen ' . $this->port,
      'access_log  /var/log/sitelogs/!ns/nginx_access.log' => 'access_log ' . drush_get_option($this->service . '-access_log'),
      'error_log  /var/log/sitelogs/!ns/nginx_error.log' => 'error_log ' . drush_get_option($this->service . '-error_log'),
      '/var/www/drupal/!ns' => $this->webroot,
    );

    if ($this->serverredirects) {
      $replacements['#server_name !redirects'] = 'server_name ' . $this->servername;
      $replacements['#rewrite ^ $scheme://example.com$request_uri? permanent'] = 'rewrite ^ $scheme://example.com$request_uri? permanent';
    }

    if ($this->ipv6_support) {
      $replacements['#listen [::]:80'] = 'listen ' . $this->ipv6_ip . ':' . $this->port;
    }

    if ($this->ssl_support) {
      $vhost .= file_get_contents(dirname(__FILE__) . '/nginx.ssl.example.com.conf');
      $replacements['!ssl_ip:443 ssl'] = $this->ssl_ip . ':' . $this->ssl_port . ' ssl';
      $replacements['/etc/ssl/certs/example-cert.pem'] = $this->ssl_cert;
      $replacements['/etc/ssl/private/example.key'] = $this->ssl_cert_key;
    }

    // Conf files are configured by default to use CGI
    // so if the setup is to proxy to apache, change things up.
    $upstream = $this->upstream;
    if ($this->upstream_type == 'apache') {
      $replacements['fastcgi_pass !upstream'] = '#fastcgi_pass ' . $upstream;
      $replacements['#proxy_pass http://!upstream'] = 'proxy_pass http://' . $upstream;
    }
    $replacements['!upstream'] = $upstream;

    $replacements['#include includes/drupal_spaces_boost.conf;'] = 'include includes/drupal_spaces_boost.conf;';

    // Automate how the vhost is constructed. This can 
    // only be done if Drupal is bootstrapped.
    /* TODO: Rework this without requiring a drupal install
    if (function_exists('module_exists')) {
      if ($purl_enabled = module_exists('purl')) {
        // Switch configuration around to use spaces conf.
        $replacements['include includes/drupal.conf;'] = '#include includes/drupal.conf;';
        $replacements['#include includes/drupal_spaces.conf;'] = 'include includes/drupal_spaces.conf;';
        if (module_exists('boost')) {
          $replacements['#include includes/drupal_spaces_boost.conf;'] = 'include includes/drupal_spaces_boost.conf;';
        }
      }
      if (!$purl_enabled && module_exists('boost')) {
        $replacements['#include includes/drupal_boost.conf;'] = 'include includes/drupal_boost.conf;';
      }
    }
    //*/
    return strtr($vhost, $replacements);
  }
}
