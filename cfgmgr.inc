<?php

/**
 * Retrieve the filepath to a service.
 */
function cfgmgr_get_service_path($service) {
  return dirname(__FILE__) . '/services';
}

/**
 * Retrive the set configuration handler.
 */
function cfgmgr_get_configuration_handler() {
  static $handlers = array();
  if (empty($handlers)) {
    $handlers = drush_command_invoke_all('cfgmgr_configuration_handlers');
  }
  if (drush_get_option('config-handler', FALSE) && isset($handlers[drush_get_option('config-handler', 'default')])) {
    $handler = $handlers[drush_get_option('config-handler', 'default')];
    if (class_exists($handler['class'])) {
      return $handler;
    }
    if (isset($handler['path'])) {
      $file = $handler['path'] . '/' . $handler['class'] . '.handler.inc';
      require_once $file;
    }
  }
  return $handlers[drush_get_option('config-handler', 'default')];
}

