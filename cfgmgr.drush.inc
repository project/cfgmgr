<?php

require_once dirname(__FILE__) . '/conf/conf.inc';
require_once dirname(__FILE__) . '/cfgmgr.services.inc';

define('CFGMGR_CONF_NO_CHANGE', 1);

define('CFGMGR_CONF_CHANGED', 2);

/**
 * Implementation of hook_drush_command().
 */
function cfgmgr_drush_command() {
  $items['cfgmgr'] = array(
    'description' => 'Retrieve a configuration output for a service in context for a Drupal envrionment.',
    'arguments' => array('cfg_ns' => 'Configuration namespace like "conf.settings.drupal"'),
    'examples' => array(
      'cfgmgr conf.vhost.nginx' => 'Return a virtual host configuration file for nginx.',
    ),
    'options' => array(
      'cfg_handler' => 'The handler to use to prompt questions to the user',
      'confdir' => 'A base path to write all configuration files to',
      'exec' => 'Execute service commands rather than output them',
      'install' => 'Action "on install" events',
      'fire-events' => 'fire "on change" events',
      'filepath' => 'The relative filepath to write the configuration file to. e.g. etc/apache/sites-available/mysite.conf',
      'ns' => 'Namespace',
    ),
    'engines' => array(
      'cfg_handler' => 'Integration with cfg handlers',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['cfgmgr-list'] = array(
    'description' => 'List all available configuration services.',
    'examples' => array(
      'cfgmgr-list' => 'List all available configuration services.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['cfgmgr-reset'] = array(
    'description' => 'Reset all variables that maybe stored in a database related to a namespace (ns)',
    'options' => array(
      'ns' => 'Namespace',
      'cfg_handler' => 'The handler to use to prompt questions to the user',
    ),
    'engines' => array(
      'cfg_handler' => 'Integration with cfg handlers',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  return $items;
}

/**
 * Callback function for Drush command: cfgmgr-reset.
 */
function drush_cfgmgr_reset() {
  drush_bootstrap_max(DRUSH_BOOTSTRAP_DRUPAL_FULL);
  if (!drush_get_option('ns', FALSE)) {
    drush_set_option('ns', 'drupal');
  }
  $handler = cfgmgr_load_handler(drush_get_option('cfg_handler', 'drushInteractive'));
  if ($handler->reset_variables(drush_get_option('ns'), $service)) {
    drush_log(drush_get_option('ns') . " configuration reset.", 'success');
  }
  else {
    drush_log(drush_get_option('ns') . " configuration reset failed.", 'error');
  }
}

/**
 * Callback function for Drush command: cfgmgr-list.
 */
function drush_cfgmgr_list() {
  drush_bootstrap_max(DRUSH_BOOTSTRAP_DRUPAL_FULL);
  $registry = drush_command_invoke_all('cfgmgr_registry');
  $output = array();
  foreach ($registry as $service => $options) {
    drush_print("[$service]");
    if (isset($options['conf'])) {
      foreach ($options['conf'] as $conf => $info) {
        drush_print("-> conf.$conf.$service", 2);
        if (isset($info['on install'])) {
          foreach ($info['on install'] as $ns) {
            drush_print("[on install] -> $ns", 6);
          }
        }
        if (isset($info['on change'])) {
          foreach ($info['on change'] as $ns) {
            drush_print("[on change] -> $ns", 6);
          }
        }
        drush_print('');
      }
    }
    if (isset($options['actions'])) {
      foreach ($options['actions'] as $action => $info) {
        drush_print("-> service.$action.$service", 2);
      }
    }
    drush_print('');
  }
}

/**
 * Callback function for Drush command: generate-config.
 */
function drush_cfgmgr($cfg_ns) {
  drush_bootstrap_max(DRUSH_BOOTSTRAP_DRUPAL_FULL);
  if (count(explode('.', $cfg_ns)) != 3) {
    return drush_set_error($cfg_ns . ' is not a valid configuration namespace');
  }
  list($style, $type, $service) = explode('.', $cfg_ns);

  // Set the namespace to drupal is not set.
  if (!drush_get_option('ns', FALSE)) {
    drush_set_option('ns', 'drupal');
  }

  // Load the service and bootstrap accordingly.
  $service = cfgmgr_registry_load($service, $type, $style);

  // Fully bootstrap unless otherwise stated. This is so that the definition
  // maybe lazyly defined.
  drush_bootstrap_to_phase(DRUSH_BOOTSTRAP_DRUPAL_FULL);

  switch ($style) {
    case 'conf':

      // Conf files require a writable directory to place the
      // files.
      if (!$conf_dir = drush_get_option('confdir', FALSE)) {
        $conf_dir = drush_prompt("Where will Drush write the contexts of $cfg_ns to?", exec('pwd'));
      }
      if ((!is_dir($conf_dir) && !mkdir($conf_dir)) || !is_writeable($conf_dir)) {
        return drush_set_error("Cannot write to configuration directory: $conf_dir");
      }
      $filepath = dirname($conf_dir . drush_get_option('filepath', $service['filepath']));
      // If the filepath starts with / then the path is absolute
      if (strpos($filepath, '/') === 0) {
        $basepath = '/';
      }
      // Otherwise the basepath is relative.
      else {
        $basepath = './';
      }
      $full_path = array_filter(explode('/', $filepath));
      $writeable_path = array();
      while (count($full_path)) {
        $writeable_path[] = array_shift($full_path);
        $filepath = $basepath . implode('/', $writeable_path);
        // If the the dir doesn't exists and can't be created OR
        // the filepath is inside of $conf_dir and not writeable
        // return an error.
        if ((!is_dir($filepath) && !mkdir($filepath)) || (strlen($conf_dir) < strlen($filepath) && !is_writeable($filepath))) {
          return drush_set_error("$filepath doesn't not exist or is not writable.");
        }
      }

      $action = cfgmgr_write_configuration($service, $conf_dir);
 
      // Fire the 'on install' event if such an event is registered and enabled.
      if (isset($service['on install']) && drush_get_option('install', FALSE)) {
        foreach ($service['on install'] as $cfg_ns) {
          drush_cfgmgr($cfg_ns);
        }
      }

      // If the configuration file has changed and the service has an on change handler,
      // call the on change handler callback.
      if ($action == CFGMGR_CONF_CHANGED && isset($service['on change']) && drush_get_option('fire-events', FALSE)) {
        foreach ($service['on change'] as $cfg_ns) {
          drush_cfgmgr($cfg_ns);
        }
      }
    break;
    case 'service':
      $output = '';
      if (isset($service['command'])) {
        if (isset($service['user'])) {
          $output .= sprintf("su %s -c '%s'", $service['user'], $service['command']); 
        }
        else {
          $output .= $service['command'];
        }
      }
      if (!drush_get_option('exec', FALSE)) {
        drush_print("# Please run command below:");
        drush_print($output,2);
      }
      else {
        $return = TRUE;
        // callbacks are only ever executed on exec.
        if (isset($service['callback'])) {
          $return = $return && call_user_func($service['callback']); 
        }
        if (!empty($output)) {
          $return = $return && drush_shell_exec_interactive($output);
        }
        drush_log($cfg_ns, $return ? 'success'  : 'error');
      }
    break;
    default:
      return drush_set_error("No such method: $style");
  }
  return;

  if ($service = cfgmgr_load_service($cfg_ns)) {
    drush_print($service->$method());
  }
}

/**
 * Callback function for Drush command: cfgmgr-simulate
 */
function drush_cfgmgr_simulate($cfg_ns) {
  $method = array_shift(explode('.', $cfg_ns));
  if ($service = cfgmgr_load_service($cfg_ns)) {
    $output = $service->$method();
    return drush_log("$service->service successfully configured", 'success');
  }
  drush_log("$service->service could not be configured", 'error');
}

/**
 * Load a service from a namespace.
 */
function cfgmgr_registry_load($service, $type, $style) {
  if ($style == 'service') {
    $style = 'actions';
  }
  $registry = drush_command_invoke_all('cfgmgr_registry');

  if (!isset($registry[$service][$style][$type])) {
    return drush_set_error("$style.$type.$service" . ' is not a supported configuration namespace');
  }
  if (isset($registry[$service]['path']) && !isset($registry[$service][$style][$type]['path'])) {
    $registry[$service][$style][$type]['path'] = $registry[$service]['path'];
  }
  $registry[$service][$style][$type]['service'] = $service;
  if (isset($registry[$service][$style][$type]['filepath'])) {
    $registry[$service][$style][$type]['filepath'] = str_replace('!ns', drush_get_option('ns'), $registry[$service][$style][$type]['filepath']);
  }

  $info = $registry[$service][$style][$type];

  if (isset($info['path'])) {
    $filename = $info['path'] . "/$style/$service.$type.inc";
    if (file_exists($filename)) {
      require_once $filename;
    }
  }
  return $info;
}

/**
 * Load a service class.
 * @param output from cfgmgr_registry_load().
 * @return A service class.
 */
function cfgmgr_load_service($service_info) {
  if (!$class = $service_info['class']) {
    return FALSE;
  }
  if (!class_exists($class)) {
    return drush_set_error("No such class exists: $class");
  }
  if (!method_exists($class, 'conf')) {
    return drush_set_error("Service class '$class' doesn't support a conf method.");
  }
  $cfg_handler = cfgmgr_load_handler(drush_get_option('cfg_handler', 'drushInteractive'));
  $cfg_handler->setService($service_info['service']);

  $service = new $class($cfg_handler);
  return $service;
}

/**
 * Load a cfg handler.
 */
function cfgmgr_load_handler($cfg_handler) {
  include_once dirname(__FILE__) . '/engines/handler.inc';
  $engines = drush_get_engines('cfg_handler');
  if (!isset($engines[$cfg_handler])) {
    return drush_set_error("No such handler: $handler");
  }
  $engine = $engines[$cfg_handler];
  $engine_includes = array($cfg_handler);
  while (isset($engine['parent'])) {
    $engine_includes[] = $engine['parent'];
    $engine = $engines[$engine['parent']];
  }
  while ($handler = array_pop($engine_includes)) {
    if (!drush_include_engine('cfg_handler', $handler)) {
      return drush_set_error("No such handler: $handler");
    }
  }

  $handler = 'cfg_handler_' . $cfg_handler;

  $cfg_handler = new $handler(drush_get_option('ns'));
  return $cfg_handler;
}

/**
 * Write the configuration file to the specified conf directory filepath.
 */
function cfgmgr_write_configuration($info, $conf_dir) {
  $info['filepath'] = drush_get_option('filepath', $info['filepath']);
  $filename = $conf_dir . $info['filepath'];
  $service = cfgmgr_load_service($info);

  if (file_exists($filename)) {
    $existing_contents = file_get_contents($filename);
    $service->pre_render();
    $contents = $service->conf($existing_contents);
    $service->post_render($contents);
    $contents = str_replace('!ns', drush_get_option('ns'), $contents);

    // If the new output is the same as the existing output,
    // don't bother to write to the file. Return a status
    // to state no change.
    if (md5($existing_contents) == md5($contents)) {
      drush_log("Configuration file " . $conf_dir . $info['filepath'] . " unchanged", 'status');
      return CFGMGR_CONF_NO_CHANGE;
    }
  }
  else {
    $contents = $service->conf(FALSE);
    $contents = str_replace('!ns', drush_get_option('ns'), $contents);
  }

  if (file_put_contents($conf_dir . $info['filepath'], $contents)) {
    drush_log("Configuration written to " . $conf_dir . $info['filepath'], 'success');
    return CFGMGR_CONF_CHANGED;
  }

  return drush_set_error("Configuration failed to write to " . $conf_dir . $info['filepath']);
}

/**
 * Implementation of command hook_drush_engine_cfg_handler().
 */
function cfgmgr_drush_engine_cfg_handler() {
  return array(
    'drushInteractive' => array(
      'path' => dirname(__FILE__) . '/engines/',
    ),
    'debconf' => array(
      'path' => dirname(__FILE__) . '/engines/',
      'parent' => 'drushInteractive',
    ),
    'preseed' => array(
      'path' => dirname(__FILE__) . '/engines/',
      'parent' => 'drushInteractive',
    ),
  );
}

/**
 * Implementation of command hook_cfgmgr_services
 */
function cfgmgr_cfgmgr_registry() {
  $basepath = dirname(__FILE__);

  $services['drupal'] = array(
    'path' => $basepath,
    'conf' => array(
      'settings' => array(
        'filepath' => 'var/www/drupal/!ns/sites/default/settings.php',
        'class' => 'cfgmgr_DrupalSettingsConf',
        'bootstrap_full' => FALSE,
        'on install' => array('service.setup.drupal'),
      ),
    ),
    'actions' => array(
      'setup' => array(
        'callback' => 'cfgmgr_drupal_setup',
      )
    ),
  );

  $services['nginx'] = array(
    'path' => $basepath,
    'conf' => array(
      'vhost' => array(
        'filepath' => 'etc/nginx/sites-available/!ns.conf',
        'class' => 'cfgmgr_NginxVhostConf',
        'on install' => array('conf.logrotated.logrotate', 'service.enable.nginx'),
        'on change' => array('service.reload.nginx'),
      ),
      'upstream' => array(
        'filepath' => 'etc/nginx/upstream.d/!ns.conf',
        'class' => 'cfgmgr_NginxUpstreamConf',
        'on change' => array('service.reload.nginx'),
      ),
    ),
    'actions' => array(
      'reload' => array(
        'command' => 'service nginx reload',
      ),
      'enable' => array(
        'callback' => 'cfgmgr_nginx_enable',
      )
    ),
  );

  $services['apache'] = array(
    'path' => $basepath,
    'conf' => array(
      'vhost' => array(
        'filepath' => 'etc/apache2/sites-available/!ns.conf',
        'class' => 'cfgmgr_ApacheVhostConf',
        'on install' => array('conf.logrotated.logrotate', 'service.enable.apache'),
        'on change' => array('service.reload.apache'),
      ),
    ),
    'actions' => array(
      'reload' => array(
        'command' => 'apache2ctl graceful',
      ),
      'enable' => array(
        'callback' => 'cfgmgr_apache_enable',
      )
    ),
  );

  $services['php-fpm'] = array(
    'path' => $basepath,
    'conf' => array(
      'pool' => array(
        'filepath' => 'etc/php5/fpm/pool.d/!ns.conf',
        'class' => 'cfgmgr_PhpfpmPoolConf',
        'on change' => array('service.restart.php-fpm'),
        'on install' => array('conf.logrotated.logrotate', 'service.create_user.php-fpm'),
      ),
    ),
    'actions' => array(
      'restart' => array(
        'command' => 'service php5-fpm restart',
      ),
      'create_user' => array(
        'callback' => 'cfgmgr_php5fpm_create_user',
      ),
    ),
  );

  $services['cron'] = array(
    'path' => $basepath,
    'conf' => array(
      'crond' => array(
        'filepath' => 'etc/cron.d/!ns',
        'class' => 'cfgmgr_CronCrondConf',
        'on install' => array('conf.logrotated.logrotate'),
        'on change' => array('service.reload.cron'),
      ),
    ),
    'actions' => array(
      'reload' => array(
        'command' => 'service cron reload',
      ),
    ),
  );

  $services['logrotate'] = array(
    'path' => $basepath,
    'conf' => array(
      'logrotated' => array(
        'filepath' => 'etc/logrotate.d/!ns',
        'class' => 'cfgmgr_LogrotateLogrotatedConf',
      ),
    ),
  );

  if (function_exists('module_exists') && module_exists('nagios')) {
    $services['nagios'] = array(
      'path' => $basepath,
      'conf' => array(
        'nrpe' => array(
          'filepath' => 'etc/nagios/nrpe.d/!ns.cfg',
          'class' => 'cfgmgr_NagiosNRPEConf',
        ),
      ),
    );
  }

  return $services;
}

